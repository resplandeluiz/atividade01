
import { StatusBar } from 'expo-status-bar';
import { useState } from 'react';
import { StyleSheet, Text, View, TextInput, SafeAreaView, TouchableOpacity, Alert } from 'react-native';

export default function App() {
  const [email, setEmail] = useState('')
  const [nome, setNome] = useState('')
  const [telefone, setTelefone] = useState('')
  const [endereco, setEndereco] = useState('')


  const onPressButton = () => Alert.alert('Dados Pessoais', `Email ${email}\nNome ${nome}\nTelefone ${telefone}\nEndereço ${endereco}`, [
    { text: 'OK', onPress: () => null },
  ]);

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="auto" />
      <View style={styles.boxTitle}>
        <Text style={styles.title}>Dados Pessoais</Text>
      </View>


      <View style={styles.boxInput}>
        <TextInput style={styles.input} onChangeText={v => setEmail(v)} placeholder='E-mail' keyboardType='email-address' autoCapitalize='none' />
        <TextInput style={styles.input} onChangeText={v => setNome(v)} placeholder='Nome' />
        <TextInput style={styles.input} onChangeText={v => setTelefone(v)} placeholder='Telefone' />
        <TextInput style={styles.input} onChangeText={v => setEndereco(v)} placeholder='Endereço' />
      </View>

      <View style={styles.boxTitle}>
        <TouchableOpacity style={styles.button} onPress={onPressButton}>
          <Text style={[styles.title, styles.titleButton]}>Enviar dados</Text>
        </TouchableOpacity>

      </View>


    </SafeAreaView >
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  input: {
    padding: 10,
    width: '100%',
    borderWidth: 1,
    borderColor: '#333',
    borderRadius: 5,
    marginBottom: 20
  },
  boxInput: {
    width: '100%',
    flex: 2,
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20
  },
  boxTitle: {
    flex: 0.5,
    justifyContent: 'center'
  },
  button: {
    backgroundColor: 'black',
    padding: 10,
    borderRadius: 5
  },
  titleButton: {
    marginBottom: 0,
    color: 'white'
  }
});
